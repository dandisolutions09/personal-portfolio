import Image from 'next/image';
import React from 'react';
import Html from '../public/assets/skills/html.png';
import Css from '../public/assets/skills/css.png';
import Javascript from '../public/assets/skills/javascript.png';
import ReactImg from '../public/assets/skills/react.png';
import Tailwind from '../public/assets/skills/tailwind.png';
import Github from '../public/assets/skills/github1.png';
import Firebase from '../public/assets/skills/firebase.png';
import NextJS from '../public/assets/skills/nextjs.png'
import AWS from '../public/assets/skills/aws.png';

import Arduino from '../public/assets/skills/arduino.png';
import Raspberrypi from '../public/assets/skills/raspberry_pi.png';
import Stm32 from '../public/assets/skills/stm32.png';
import Espressif from '../public/assets/skills/espressif.png';
import Esp8266 from '../public/assets/skills/esp8266.png';
import Esp32 from '../public/assets/skills/esp32_logo.png';
import Linux from '../public/assets/skills/linux_embedded.png';
import Microchip from '../public/assets/skills/microchip.png';
import Nordic from '../public/assets/skills/nordic.png';
import pi_pico from '../public/assets/skills/pi_pico.png';
import Ros from '../public/assets/skills/ros_logo.png';



import Cpp from '../public/assets/skills/Cpp.png';
import Python from '../public/assets/skills/python_logo.png';
import C from '../public/assets/skills/C.png';
import  Golang from '../public/assets/skills/Golang.png';
import  DigitalOcean from '../public/assets/skills/digitalocean.png';






const Skills = () => {
  return (
    <div id='skills' className='w-full lg:h-screen p-2'>
      <div className='max-w-[1240px] mx-auto flex flex-col justify-center h-full'>
        <p className='text-xl tracking-widest uppercase text-[#5651e5]'>
          Skills
        </p>
        <h2 className='py-4'>What I Can Do (Professional level)</h2>
        <div className='grid grid-cols-2 lg:grid-cols-4 gap-8'>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Arduino} width='104px' height='64px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Arduino</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Raspberrypi} width='94px' height='64px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Raspberry Pi</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Stm32} width='64px' height='64px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>STM32</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Esp32} width='84px' height='84px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Esp32</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Nordic} width='164px' height='164px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Nordic NRF</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Microchip} width='304px' height='194px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Microchip</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Linux} width='164px' height='84px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Linux</h3>
              </div>
            </div>
          </div>
          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Ros} width='124px' height='94px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>ROS</h3>
              </div>
            </div>
          </div>

          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Espressif} width='354px' height='154px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Espressif</h3>
              </div>
            </div>
          </div>


          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Python} width='254px' height='154px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Python</h3>
              </div>
            </div>
          </div>



          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Cpp} width='154px' height='154px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>C++</h3>
              </div>
            </div>
          </div>


          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={C} width='174px' height='104px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>C</h3>
              </div>
            </div>
          </div>


          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={Golang} width='174px' height='104px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Golang(Go)</h3>
              </div>
            </div>
          </div>



          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={DigitalOcean} width='174px' height='104px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Digital Ocean</h3>
              </div>
            </div>
          </div>


          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={AWS} width='94px' height='104px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>Amazon Web Services</h3>
              </div>
            </div>
          </div>



          <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
            <div className='grid grid-cols-2 gap-4 justify-center items-center'>
              <div className='m-auto'>
                <Image src={ReactImg} width='94px' height='104px' alt='/' />
              </div>
              <div className='flex flex-col items-center justify-center'>
                <h3>React</h3>
              </div>
            </div>
          </div>







        </div>
      </div>
    </div>
  );
};

export default Skills;
