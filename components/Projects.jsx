import Image from "next/image";
import Link from "next/link";
import React from "react";
import HousePrinter from "../public/assets/projects/house_printer.jpg";
import LED_rev_indicator from "../public/assets/projects/LED3.png";
import Cooler_box from "../public/assets/projects/cooler.jpg";
import Cattle_tracker from "../public/assets/projects/cattle_tracker.jpg";
import Pedometer from "../public/assets/projects/pedometer_nrf.jpg";
import PCB_design from "../public/assets/projects/PCB_design.jpg";
import GPS_tracker from "../public/assets/projects/GPS_tracker.jpg";


import ProjectItem from "./ProjectItem";

const Projects = () => {
  return (
    <div id="projects" className="w-full">
      <div className="max-w-[1240px] mx-auto px-2 py-16">
        <p className="text-xl tracking-widest uppercase text-[#5651e5]">
          Projects
        </p>
        <h2 className="py-4">What I&apos;ve Built</h2>
        <div className="grid md:grid-cols-2 gap-8">
          <ProjectItem
            title="3D house Printer"
            backgroundImg={HousePrinter}
            projectUrl="/property"
            tech="Mechatronics Engineering"
          />
          <ProjectItem
            title="LED Rev Indicator"
            backgroundImg={LED_rev_indicator}
            projectUrl="/crypto"
            tech="C++"
          />
          <ProjectItem
            title="Solar Based Cooler Box"
            backgroundImg={Cooler_box}
            projectUrl="/netflix"
            tech="C++"
          />
          <ProjectItem
            title="Solar-Based Cattle GPS Tracker"
            backgroundImg={Cattle_tracker}
            projectUrl="/twitch"
            tech="C++"
          />

          <ProjectItem
            title="Smartwatch Pedometer"
            backgroundImg={Pedometer}
            projectUrl="/twitch"
            tech="NRF SDK"
          />

          <ProjectItem
            title="GPS Tracker"
            backgroundImg={GPS_tracker}
            projectUrl="/twitch"
            tech="C++/ Arduino"
          />

          <ProjectItem
            title="PCB for LED Rev indicator"
            backgroundImg={PCB_design}
            projectUrl="/twitch"
            tech="KiCAD"
          />
        </div>
      </div>
    </div>
  );
};

export default Projects;
