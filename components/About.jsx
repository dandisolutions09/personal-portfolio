import React from "react";
import Image from "next/image";
import Link from "next/link";
import AboutImg from "../public/assets/profile_pic.jpg";

const About = () => {
  return (
    <div id="about" className="w-full md:h-screen p-2 flex items-center py-16">
      <div className="max-w-[1240px] m-auto md:grid grid-cols-3 gap-8">
        <div className="col-span-2">
          <p className="uppercase text-xl tracking-widest text-[#5651e5]">
            About
          </p>
          <h2 className="py-4">Who I Am</h2>
          <p className="py-2 text-gray-600">
            Greetings! I'm Don Collins, an Embedded Software Engineer with a broad
            range of expertise, including low-level technologies, transmission
            protocols, IoT development, and familiarity with various
            System-on-Chip (SoC) platforms. If you require assistance with
            projects involving different SoCs, you've come to the right place.
          </p>
          <p className="py-2 text-gray-600">
            I have hands-on experience working with a variety of SoCs, enabling
            me to tailor solutions to your specific requirements. Some of the
            SoCs I am familiar with include:
          </p>

          <p className="py-2 text-gray-600">
            - ARM Cortex Chips: I possess knowledge and expertise in programming
            ARM Cortex-based microcontrollers and processors. Whether you're
            working with Cortex-M0, Cortex-M3, Cortex-M4, or other variants, I
            can help you leverage the full potential of these powerful chips.
          </p>

          <p className="py-2 text-gray-600">
            - Nordic NRF chips: Nordic Semiconductor's NRF series offers a range
            of versatile and feature-rich SoCs. I have familiarity with
            programming NRF chips, enabling seamless integration of these chips
            into your projects, whether it's for wireless communication, IoT
            applications, or other functionalities
          </p>

          <p className="py-2 text-gray-600">
            - ESP32s and ESP8266: These popular SoCs from Espressif Systems are
            widely used for IoT applications and Wi-Fi-enabled devices. I am
            proficient in programming both ESP32 and ESP8266 chips, allowing me
            to create robust and reliable solutions for your IoT projects.
          </p>

          <p className="py-2 text-gray-600">
            My familiarity with these SoCs extends beyond basic programming. I
            understand their capabilities, hardware integration nuances, and how
            to optimize their performance for your specific requirements.
          </p>

          <p className="py-2 text-gray-600">
            I am passionate about delivering high-quality results and staying
            up-to-date with the latest advancements in technology. With my
            knowledge of SoCs, low-level programming, transmission protocols,
            and IoT development, I am well-equipped to tackle your most
            challenging projects.
          </p>

          <p className="py-2 text-gray-600">
            If you're seeking an experienced professional who can leverage the
            capabilities of various SoCs to bring your ideas to life, look no
            further. Let's collaborate and create innovative solutions using the
            power of these advanced chips. Feel free to reach out, and together
            we can achieve remarkable outcomes.
          </p>

          <Link href="/#projects">
            <p className="py-2 text-gray-600 underline cursor-pointer">
              Check out some of my latest projects.
            </p>
          </Link>
        </div>
        <div className="w-full h-auto m-auto shadow-xl shadow-gray-400 rounded-xl flex items-center justify-center p-4 hover:scale-105 ease-in duration-300">
          <Image src={AboutImg} className="rounded-xl" alt="/" />
        </div>
      </div>
    </div>
  );
};

export default About;
